#!/usr/bin/env bash

# ADD MAILDIR
source /etc/profile.d/mail.sh

# START LOGGING SYSTEM
service syslog-ng start

# START POSTFIX
service postfix start

# INIT MAIL CLIENT
echo 'init' | mail -s 'init' -Snorecord root

# SEND TEST MESSAGE
cat /etc/postfix/test_msg.txt | mail -s "MailRelay - New service created $( hostname -f )" skiel.j.lopez@gmail.com

# TO KEEP CONTAINER RUNNING
tail -f /var/log/mail.log
