#!/usr/bin/env bash

hostname saltmaster1 &&

cp /etc/hosts /tmp/hosts && cp /etc/hostname /tmp/hostname && \

hn=$(cat /etc/hostname) &&

sed -i "s|$hn|saltmaster1|g" /tmp/hostname &&
sed -ri 's|(127\.0\.0\.1).+$|\1 saltmaster1.myastute.local saltmaster1 localhost|g' /tmp/hosts &&
sed -i "s|$hn|saltmaster1|g" /tmp/hosts &&

cp -f /tmp/hosts /etc/hosts && cp -f /tmp/hostname /etc/hostname

/usr/bin/salt-master -l debug
