#
# Cookbook:: learn_chef_apache2
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.
apt_update 'Update the apt cache daily' do
  frequency 86_400
  action :periodic
end

package 'apache2'

template '/tmp/fuse_html.conf' do
  source 'fuse_html.conf' 
end

bash 'change_defaul_index_page' do
  user 'root'
  code <<-EOF
    cat /tmp/fuse_html.conf >> /etc/apache2/apache2.conf
    rm /tmp/fuse_html.conf
    service apache2 restart
  EOF
  not_if 'grep -q "<Directory /var/www/html>" /etc/apache2/apache2.conf'
end

service 'apache2' do
  supports status: true
  action [:enable, :start]
end

template '/var/www/html/fuse.html' do
  source 'index.html.erb'
end
