# ==============================================================================
# BKP ROUTE53
# -----------
# Author: Ezequiel Lopez <skiel.j.lopez@gmail.com>
# Date: 2017-01-25
# Description: Reads the hosted zone configuration and generates BIND files to
# backup the resources of each zone
# ==============================================================================

from datetime import datetime
import boto3
import tempfile as tmpf

# import this for debug
#import pprint 

RR_LINE_FORMATS = {
    'A': lambda rr: ['{name}\t{ttl}\tIN\t{type}\t{value}\n'.format(
                name  = rr['Name'],
                ttl   = rr['TTL'],
                type  = rr['Type'],
                value = ele['Value']
            ) for ele in rr['ResourceRecords']
        ],

    'AA': lambda rr: '{name}\t{ttl}\tIN\t{type}\t{value}\n'.format(
            name  = rr['Name'],
            ttl   = '',
            type  = rr['Type'],
            value = 'ALIAS {dns_name} ({hz_id})'.format(
                dns_name = rr['AliasTarget']['DNSName'],
                hz_id    = rr['AliasTarget']['HostedZoneId'].lower()
            ),
        ),

    'CNAME': lambda rr: ['{name}\t{ttl}\tIN\t{type}\t{value}\n'.format(
                name  = rr['Name'],
                ttl   = rr['TTL'],
                type  = rr['Type'],
                value = ele['Value']
            ) for ele in rr['ResourceRecords']
        ],

    'NS': lambda rr: ['{name}\t{ttl}\tIN\t{type}\t{value}\n'.format(
                name  = rr['Name'],
                ttl   = rr['TTL'],
                type  = rr['Type'],
                value = ele['Value']
            ) for ele in rr['ResourceRecords']
        ],

    'TXT': lambda rr: ['{name}\t{ttl}\tIN\t{type}\t{value}\n'.format(
                name  = rr['Name'],
                ttl   = rr['TTL'],
                type  = rr['Type'],
                value = ele['Value']
            ) for ele in rr['ResourceRecords']
        ],

    'SOA': lambda rr: ['{name}\t{ttl}\tIN\t{type}\t{value}\n'.format(
                name  = rr['Name'],
                ttl   = rr['TTL'],
                type  = rr['Type'],
                value = ele['Value']
            ) for ele in rr['ResourceRecords']
        ],

    'MX': lambda rr: ['{name}\t{ttl}\tIN\t{type}\t{value}\n'.format(
                name  = rr['Name'],
                ttl   = rr['TTL'],
                type  = rr['Type'],
                value = ele['Value'].replace(' ', '\t')
            ) for ele in rr['ResourceRecords']
        ],

    'SRV': lambda rr: ['{name}\t{ttl}\tIN\t{type}\t{value}\n'.format(
                name  = rr['Name'],
                ttl   = rr['TTL'],
                type  = rr['Type'],
                value = ele['Value'].replace(' ', '\t')
            ) for ele in rr['ResourceRecords']
        ],
}

def get_resources(route53, **kwargs):
    data = route53.list_resource_record_sets(**kwargs)
    if data['IsTruncated']:
        kwargs['StartRecordName']   = data['NextRecordName']
        kwargs['StartRecordType']   = data['NextRecordType']
        data['ResourceRecordSets'] += get_resources(route53, **kwargs)['ResourceRecordSets']
    return data

def build_upload_file(bucket, data, current_date):
    f = tmpf.NamedTemporaryFile()
    f.write('$ORIGIN {}\n'.format(data['Name']))
    for res in data['ResourceRecordSets']:
        if 'ResourceRecords' in res:
            f.write(''.join(RR_LINE_FORMATS[res['Type']](res)))
        if 'AliasTarget' in res:
            f.write(RR_LINE_FORMATS['AA'](res))
    key = 'route53/{date}.{h_zone}txt'.format(
        date   = current_date,
        h_zone = data['Name']
    )
    f.flush()
    bucket.upload_file(f.name, key)
    f.close()

def lambda_handler(event, context):
    #pp = pprint.PrettyPrinter(indent=2)
    current_date = datetime.now().strftime('%Y%m%d')
    s3     = boto3.resource('s3')
    bucket = s3.Bucket('bucket-name')
    old_files = { 
        'Objects': [
            { 'Key': str(obj.key) } for obj in bucket.objects.filter(Prefix='route53/')
        ]
    }
    if old_files['Objects']:
        bucket.delete_objects(Delete=old_files)
    route53 = boto3.client('route53')
    h_zones = route53.list_hosted_zones()['HostedZones']
    for h_zone in h_zones:
        hz_id = h_zone['Id'].split('/')[-1]
        h_zone['ResourceRecordSets'] = get_resources(route53, HostedZoneId=hz_id)['ResourceRecordSets']
        build_upload_file(bucket, h_zone, current_date)
    return True
